# -*- coding: utf-8 -*-
"""
Created on Thu Jan  2 15:00:53 2020

@author: Tom
"""

import numpy as np

_default_x_size = 10.
_default_y_size = 10.
_default_world_type = 'periodic'
_allowed_world_types = ['periodic', 'reflective']

x_size = _default_x_size
y_size = _default_y_size
world_type = _default_world_type

def set_x_size(size: float):
    global x_size
    x_size = size

def set_y_size(size: float):
    global y_size
    y_size = size
    
def set_world_type(new_type: str):
    try:
        if new_type in _allowed_world_types:
            global world_type
            world_type = new_type
        else:
            raise ValueError("invalid_world_type")
    except ValueError as error:
        if str(error) == "invalid_world_type":
            print("ValueError: invalid world type '{}' used.".format(new_type))
        else:
            raise error
            
def _handle_periodic_boundaries(coords: np.array):
    coords[0] = coords[0] % x_size
    coords[1] = coords[1] % y_size
    
def _handle_reflective_boundaries(coords: np.array):
    if coords[0] < 0:
        coords[0] = min(-coords[0], x_size/2)
    elif coords[0] >= x_size:
        coords[0] = max(-coords[0] + 2*x_size, x_size/2)
        
    if coords[1] < 0:
        coords[1] = min(-coords[1], y_size/2)
    elif coords[1] >= y_size:
        coords[1] = max(-coords[1] + 2*y_size, y_size/2)
        
_boundary_methods = {
    "periodic": _handle_periodic_boundaries,
    "reflective": _handle_reflective_boundaries
    }

def handle_boundaries(coords: np.array):
    _boundary_methods[world_type](coords)


    
    
    
    
    